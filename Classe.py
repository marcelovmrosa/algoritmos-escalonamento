__author__ = 'Marcelo'
import random

class PCB():
    def __init__(self, num=None, burst=None, wait=None, turn=None, came=None, priority=None):
        self.num = num
        self.burst = burst
        self.wait = wait
        self.turn = turn
        self.came = came
        self.priority = priority

def fcfs(processo):
    processo[0].wait=0
    for x in range(len(processo)):
        b = 0
        for i in range(0, x):
            b += processo[i].burst
        processo[x].wait = b
        processo[x].turn = processo[x].wait + processo[x].burst
    return processo

def sjf(processo):
    processo = sorted(processo, key=lambda PCB: PCB.burst)
    processo[0].wait=0
    for x in range(len(processo)):
        b = 0
        for i in range(0, x):
            b += processo[i].burst
        processo[x].wait = b
        processo[x].turn = processo[x].wait + processo[x].burst
    return processo

def srtf(processo, back_up):
    select = 0
    while True:
        select = str(input("Definir tempo de chegada? sim[s] não[n]"))
        if select == 's' or select == 'n':
            break
    if select == 's':
        for x in range(len(processo)):
            processo[x].came = int(input("P[%d] = "%processo[x].num))
    else:
        for x in range(len(processo)):
            processo[x].came = processo[x].num
    processo = sorted(processo, key=lambda PCB: PCB.came)
    wait = []
    for x in range(0,len(processo)):
            processo[x].turn = 0
            processo[x].wait = 0
            if processo[x].came == 0:
                wait.append(processo[x])
    for y in range(0, (burst(processo)+1)):
        for x in range(1, len(processo)):
            if y == processo[x].came:
                wait.append(processo[x])
        wait = sorted(wait, key=lambda PCB: PCB.burst)
        for x in range(len(wait)):
            if wait[x].burst != 0:
                wait[x].burst -= 1
                wait[x].turn += 1
                if len(wait) > 1 and x < (len(wait)-1):
                    for z in range(x+1, len(wait)):
                        wait[z].wait += 1
                break
    wait = sorted(wait, key=lambda PCB: PCB.num)
    processo = sorted(processo, key=lambda PCB: PCB.num)
    for x in range(len(processo)):
        processo[x].burst = back_up[x].burst
        processo[x].turn = wait[x].wait + wait[x].turn
    processo = sorted(processo, key=lambda PCB: PCB.came)
    return processo

def rr(processo, back_up):
    quantum = int(input("Definir tempo de quantum: "))
    for x in range(len(processo)):
        processo[x].turn = 0
        processo[x].wait = 0
    while(burst(processo) != 0):
        for x in range(0,len(processo)):
            if processo[x].burst != 0:
                k = 0
                while(k < quantum):
                    if processo[x].burst != 0:
                        processo[x].burst -= 1
                        processo[x].turn += 1
                        for y in range(0, len(processo)):
                            if processo[x].num != processo[y].num:
                                if processo[y].burst != 0:
                                    processo[y].wait += 1
                    else:
                        break
                    k += 1
    for x in range(len(processo)):
        processo[x].turn = processo[x].wait + processo[x].turn
        processo[x].burst = back_up[x].burst
    return processo

def wait(processo):
    a = 0
    for x in range(len(processo)):
        a += processo[x].wait
    return a

def burst(processo):
    a = 0
    for x in range(len(processo)):
        a += processo[x].burst
    return a

def turn(processo):
    a = 0
    for x in range(len(processo)):
        a += processo[x].turn
    return a

while True:
    processo = []
    back_up = []
    select = 0
    num_processo = int(input("Insira número de processos: "))
    while True:
        select = int(input("Insira Burst(aleatório[1]/manual[2]: "))
        if select == 1 or select == 2:
            break
    if select == 2:
        for x in range(num_processo):
            processo.append(PCB())
            back_up.append(PCB())
            num_burst = int(input("P[%d] = "%x))
            processo[x].burst = num_burst
            back_up[x].burst = num_burst
            processo[x].num = x
    else:
        for x in range(num_processo):
            processo.append(PCB())
            back_up.append(PCB())
            num_burst = random.randint(1, 20)
            print("P[%d] = %d" % (x, num_burst))
            processo[x].burst = num_burst
            back_up[x].burst = num_burst
            processo[x].num = x

    while True:
        select = int(input("Escolha o algoritmo de escalonamento\n[1] FCFS\n[2] SJF\n[3] SRTF\n[4] RR\n[#] = "))
        if select == 1 or select == 2 or select == 3 or select == 4:
            break

    if select == 1:
        processo = fcfs(processo)
    elif select == 2:
        processo = sjf(processo)
    elif select == 3:
        processo = srtf(processo, back_up)
    else:
        processo = rr(processo, back_up)


    print("\tProcesso\tBurst\tWaiting Time\tTurnaround\t Tempo de chegada\tPrioridade")
    for x in range(num_processo):
        print("\t P[%d]\t\t %s\t\t\t %s\t\t\t    %s\t\t\t   %s\t\t\t %s" % (processo[x].num, processo[x].burst, processo[x].wait, processo[x].turn, processo[x].came, processo[x].priority))
    print("Waiting Time médio: ", (wait(processo))//num_processo)
    print("Turnaround Time médio: ", (turn(processo))//num_processo)

    while True:
        select = str(input("\nEncerrar programa(sim[s]/não[n]: "))
        if select == 's' or select == 'n':
            break
    if select == 'n':
        print("PROGRAMA REINICIADO!\n")
        continue
    else:
        print("PROGRAMA TERMINADO!")
        break